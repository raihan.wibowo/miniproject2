# MiniProject2

Membuat Chatapp

# Requirements

## Python version

python = 3.8.10
django =4.0.2
docker
redis

dapat dilakukan "pip install -r requirements.txt" untuk dependenciesnya

# Step menjalankan

1. clone atau download program pada gitlab
2. masuk ke virtualenvironment (bila diperlukan) dengan "poetry shell", atau langsung ke step ke-3
3. jalankan "docker run -p 6379:6379 -d redis:5" agar dapat dijalankan oleh multiple user
4. jalankan "python manage.py runserver"
5. buka localhost:8000/chat , kemudian kita akan diminta untuk melakukan login.
6. pilih nama room chat yang diinginkan
7. ruang chat terbuka, coba buka new tab dengan alamat localhost:8000/chat/<roomName> yang sama
8. lakukan chat dari kedua tab



